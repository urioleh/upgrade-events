window.onload = () => {
    btnClick();
    inputFocus();
    inputInput();
}




// 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el 
// evento click que ejecute un console log con la información del evento del click
const btnClick = () => {
    var btnRecover = document.getElementById('btnToClick');
    btnRecover.addEventListener('click', clickRecover);

}


const clickRecover = () => {
    const recover = document.querySelector('.click')
    console.log(recover.value)
}

// 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.
const inputFocus = () => {
    document.querySelector('.focus').addEventListener('focus', focusRecover);
}

const focusRecover = () => {
    const recover = document.querySelector('.focus');
    console.log(recover.value)
}
// 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.

const inputInput = () => {
    document.querySelector('.value').addEventListener('input', inputRecover);
}

const inputRecover = () => {
    const recover = document.querySelector('.value');
    console.log(recover.value)
}